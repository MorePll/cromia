using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public int initialMaxEnemies = 10;
    public float spawnInterval = 1f;
    public float increaseInterval = 10f; // Intervalo para aumentar el m�ximo de enemigos
    public GameObject player;
    public GameObject shield;
    public ShieldCollisionCounter shieldCollisionCounter;
    public PlayerLivesManager livesManager;
    public AudioSource deathSound;
    public AudioSource damageSound;

    private int maxEnemies;
    private float spawnTimer = 0f;
    private float increaseTimer = 0f;

    void Start()
    {
        maxEnemies = initialMaxEnemies;
    }

    void Update()
    {
        // Actualizar los temporizadores
        spawnTimer += Time.deltaTime;
        increaseTimer += Time.deltaTime;

        // Incrementar el m�ximo de enemigos cada 'increaseInterval' segundos
        if (increaseTimer >= increaseInterval)
        {
            maxEnemies++;
            increaseTimer = 0f;
        }

        // Si a�n no alcanzamos el l�mite de enemigos y ha pasado el intervalo de spawn
        if (GameObject.FindGameObjectsWithTag("Enemy").Length < maxEnemies && spawnTimer >= spawnInterval)
        {
            SpawnEnemy();
            // Reiniciar el temporizador
            spawnTimer = 0f;
        }
    }

    void SpawnEnemy()
    {
        // Instanciar un nuevo enemigo en la posici�n del spawner
        GameObject newEnemy = Instantiate(enemyPrefab, transform.position, Quaternion.identity);
        EnemyBehavior enemyBehavior = newEnemy.GetComponent<EnemyBehavior>();
        enemyBehavior.SetReferences(player, shield, shieldCollisionCounter, livesManager, deathSound, damageSound);
    }
}