using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class materialesInteractivos : MonoBehaviour
{
    public Renderer miSkin; // Objeto Renderer �nico
    Material miMaterialOriginal; // Material original para el objeto
    ParticleSystem particula;
    
    void Start()
    {
        // Guardar el material original del objeto miSkin
        miMaterialOriginal = miSkin.material;
    }

    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("cambioDeMaterial"))
        {
            // Obtener el componente materialACambiar del objeto con el que se colision�
            materialACambiar materialScript = other.gameObject.GetComponent<materialACambiar>();
            particula = materialScript.particle;

            // Verificar si se encontr� el componente y si el materialDeCambio est� asignado
            if (materialScript != null && materialScript.materialDeCambio != null)
            {
                // Cambiar el material del objeto miSkin
                miSkin.material = materialScript.materialDeCambio;
                particula.Play();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("cambioDeMaterial"))
        {
            particula.Stop();
        }
    }
}
