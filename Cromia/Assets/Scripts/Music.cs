using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public AudioSource audioSource;
    private bool isPaused = false;

    /*void Start()
    {
        audioSource = GetComponent<AudioSource>();

        // Configurar el AudioSource
        audioSource.loop = true;
        audioSource.playOnAwake = true;

        // Reproducir la m�sica
        audioSource.Play();
    }*/

    void LateUpdate()
    {
        if (Time.timeScale == 0 && !isPaused)
        {
            audioSource.Pause();
            isPaused = true;
        }
        else if (Time.timeScale > 0 && isPaused)
        {
            audioSource.UnPause();
            isPaused = false;
        }
    }
}
