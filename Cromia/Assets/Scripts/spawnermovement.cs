using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnermovement : MonoBehaviour
{
    public Transform player;
    public float rotationSpeed = 30f;
    public float orbitRadius = 2f;

    private float currentAngle = 0f; // �ngulo de rotaci�n

    void Update()
    {
        // Calcular la posici�n deseada del objeto alrededor del jugador
        float x = player.position.x + Mathf.Cos(currentAngle) * orbitRadius;
        float z = player.position.z + Mathf.Sin(currentAngle) * orbitRadius;
        Vector3 desiredPosition = new Vector3(x, transform.position.y, z);

        // Actualizar la posici�n del objeto
        transform.position = desiredPosition;

        // Incrementar el �ngulo de rotaci�n
        currentAngle += rotationSpeed * Time.deltaTime;
        if (currentAngle >= 360f)
        {
            currentAngle -= 360f;
        }
    }
}