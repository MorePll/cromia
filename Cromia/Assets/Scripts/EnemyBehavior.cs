using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    public GameObject shield;
    public GameObject player;
    public Material[] enemyMaterials;
    public int minSpeed = 10;
    public int maxSpeed = 30;
    public AudioSource deathSound;
    public AudioSource damageSound;
    public ShieldCollisionCounter shieldCollisionCounter;
    public PlayerLivesManager livesManager;

    private int speed;

    void Start()
    {
        // Inicializar la velocidad aleatoria del enemigo
        speed = Random.Range(minSpeed, maxSpeed + 1); // +1 al valor m�ximo para incluirlo en el rango

        // Seleccionar un material aleatorio para el enemigo
        int randomMaterialIndex = Random.Range(0, enemyMaterials.Length);
        Renderer enemyRenderer = GetComponent<Renderer>();
        enemyRenderer.material = enemyMaterials[randomMaterialIndex];
    }

    void Update()
    {
        // Calcular la direcci�n hacia el jugador en cada frame
        Vector3 direction = (player.transform.position - transform.position).normalized;

        // Establecer la direcci�n solo en los planos X y Z, dejando Y sin cambios
        direction.y = 0;

        // Mover al enemigo hacia el jugador con la velocidad actual
        transform.Translate(direction * speed * Time.deltaTime);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (damageSound != null)
            {
                // Reproducir el sonido de da�o
                damageSound.Play();
            }
            Destroy(gameObject);
            livesManager.DecreaseLife();
            return; // Salir de la funci�n para evitar comprobaciones adicionales
        }

        // Obtener el material del escudo
        Renderer shieldRenderer = shield.GetComponent<Renderer>();
        Material shieldMaterial = shieldRenderer.sharedMaterial;

        // Obtener los materiales del enemigo
        Renderer enemyRenderer = GetComponent<Renderer>();
        Material[] enemyMaterials = enemyRenderer.sharedMaterials;

        // Verificar si alguno de los materiales del enemigo coincide con el material del escudo
        foreach (Material enemyMaterial in enemyMaterials)
        {
            if (enemyMaterial.name == shieldMaterial.name && other.gameObject.CompareTag("Shield"))
            {
                DestroyEnemy();
                return; // Salir del bucle foreach
            }
        }
    }

    void DestroyEnemy()
    {
        if (deathSound != null)
        {
            // Reproducir el sonido de muerte
            deathSound.Play();
        }

        Destroy(gameObject);
        shieldCollisionCounter.IncrementCollisionCount();
    }

    public void SetReferences(GameObject player, GameObject shield, ShieldCollisionCounter shieldCollisionCounter, PlayerLivesManager livesManager, AudioSource deathSound, AudioSource damageSound)
    {
        this.player = player;
        this.shield = shield;
        this.shieldCollisionCounter = shieldCollisionCounter;
        this.livesManager = livesManager;
        this.deathSound = deathSound;
        this.damageSound = damageSound;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Floor"))
        {
            Destroy(gameObject);
        }
    }
}
