using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerLivesManager : MonoBehaviour
{
    public Image[] lifeImages; // Array de im�genes para representar las vidas.
    public TextMeshProUGUI gameOverText; // Texto de UI para mostrar "Game Over".
    public int maxLives = 5; // M�ximo de vidas permitidas.
    private int currentLives; // Vidas actuales del jugador.

    void Start()
    {
        currentLives = Mathf.Clamp(3, 0, maxLives);
        UpdateLifeUI();
        gameOverText.enabled = false;
    }

    public void DecreaseLife()
    {
        if (currentLives > 0)
        {
            currentLives--;
            UpdateLifeUI();

            if (currentLives <= 0)
            {
                EndGame();
            }
        }
    }

    public void IncreaseLife()
    {
        if (currentLives < maxLives)
        {
            currentLives++;
            UpdateLifeUI();
        }
    }

    private void UpdateLifeUI()
    {
        for (int i = 0; i < lifeImages.Length; i++)
        {
            if (i < currentLives)
            {
                lifeImages[i].enabled = true; // Mostrar la imagen de vida.
            }
            else
            {
                lifeImages[i].enabled = false; // Ocultar la imagen de vida.
            }
        }
    }

    private void EndGame()
    {
        Time.timeScale = 0; // Pausar el juego.
        gameOverText.enabled = true; // Mostrar el texto de "Game Over".
    }
}
