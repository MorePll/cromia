using UnityEngine;
using TMPro;

public class ShieldCollisionCounter : MonoBehaviour
{
    private int collisionCount = 0;
    private TextMeshProUGUI textMeshPro;

    void Start()
    {
        textMeshPro = GetComponent<TextMeshProUGUI>();
        UpdateCounterText();
    }

    // M�todo para incrementar el contador de colisiones
    public void IncrementCollisionCount()
    {
        collisionCount++;
        UpdateCounterText();
    }

    // M�todo para actualizar el texto del contador
    private void UpdateCounterText()
    {
        // Formatear el contador como 00
        string formattedCount = collisionCount.ToString("00");
        // Actualizar el texto del componente TextMeshPro
        textMeshPro.text = formattedCount;
    }
}
