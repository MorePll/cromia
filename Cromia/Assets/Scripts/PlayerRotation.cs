using UnityEngine;

public class PlayerRotation : MonoBehaviour
{
    public float rotationSpeed = 1000f; // Velocidad de rotaci�n en grados por segundo

    private Vector3[] squarePositions; // Direcciones de rotaci�n para cada lado del cuadrado
    private int currentTargetIndex = 90; // �ndice del lado del cuadrado al que se dirige el personaje

    void Start()
    {
        // Definir las direcciones de rotaci�n para cada lado del cuadrado
        squarePositions = new Vector3[]
        {
            Vector3.forward,    // Lado superior
            Vector3.right,      // Lado derecho
            Vector3.back,       // Lado inferior
            Vector3.left        // Lado izquierdo
        };
    }

    void Update()
    {
        // Control de rotaci�n por el jugador
        if (Input.GetButtonDown("Fire1")/* || Input.GetKeyDown(KeyCode.Q)*/)
        {
            // Girar hacia la izquierda
            currentTargetIndex--;
            if (currentTargetIndex < 0)
                currentTargetIndex = squarePositions.Length - 1;
        }
        else if (Input.GetButtonDown("Fire2")/* || Input.GetKeyDown(KeyCode.E)*/)
        {
            // Girar hacia la derecha
            currentTargetIndex++;
            if (currentTargetIndex >= squarePositions.Length)
                currentTargetIndex = 0;
        }

        //Asegurar que el �ndice est� dentro de los l�mites del array
        currentTargetIndex = Mathf.Clamp(currentTargetIndex, 0, squarePositions.Length - 1);
        //Calcular la direcci�n hacia el lado del cuadrado actual
        Vector3 direction = squarePositions[currentTargetIndex];
        // Rotar hacia la direcci�n deseada
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
    }
}